extends RigidBody3D
class_name Body

@rpc("any_peer" ,"call_local", "unreliable")
func push(impulse: Vector3):
	apply_central_impulse(impulse)
