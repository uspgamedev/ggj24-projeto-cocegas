extends Body
class_name TiltingDoll


@export var SPEED = 50
@export var JUMP_VELOCITY = 20.
@export var JUMP_WALK_VELOCITY = 2.

@export var RECOVER_ELASTICITY := 100 # 30
@export var HORIZONTAL_SENSITIVITY = 5
@export var JOYSTICK_HORIZONTAL_SENSIBILITY = 2.0

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@export var model : Model
@export var camera_pivot: Node3D

signal start_walking()
signal stopped()
signal start_jumping()

var is_stopped = false
	
var horizontal_move_buffer = []
	
func _unhandled_input(event):
	if not is_multiplayer_authority():
		return
		
	if event is InputEventMouseMotion:
		#angular_velocity.y += deg_to_rad(-event.relative.x * HORIZONTAL_SENSITIVITY)
		horizontal_move_buffer.push_back(deg_to_rad(-event.relative.x * HORIZONTAL_SENSITIVITY))
	
	#if event is InputEventJoypadMotion and event.axis == 2:
		#angular_velocity.y = deg_to_rad(-event.axis_value * 200.)

@export var direction := Vector3()

var prev_clown = false

func _physics_process(delta):
	update_input()
	update_movement(delta)
	
	var aux = (%Head.global_position - %Foot.global_position)
	aux.y = 0
	apply_force(-aux*RECOVER_ELASTICITY, %Head.position)
	
	if is_stopped:
		linear_velocity = Vector3()
		angular_velocity = Vector3()
	
func update_input():
	angular_velocity.y = 0
	if not is_multiplayer_authority() or Menu.get_menu().is_paused():
			return

	if Input.is_action_just_pressed("jump") and is_on_floor():
		apply_central_impulse(transform.basis * Vector3.UP * JUMP_VELOCITY * mass)

	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	
	angular_velocity.y += -Input.get_axis("camera_left", "camera_right")*JOYSTICK_HORIZONTAL_SENSIBILITY
	
	while not horizontal_move_buffer.is_empty():
		angular_velocity.y += horizontal_move_buffer.pop_back()
	#if Input.get_connected_joypads().size() > 0:
		#var camera_movement = -Input.get_axis("camera_left", "camera_right")
		#angular_velocity.y = camera_movement

var impulses = []

var is_walking := false

func update_movement(_delta):
	if impulses.is_empty():
		if not direction.is_zero_approx():
			if not is_walking:
				start_walking.emit()
				is_walking = true
				
			apply_central_force(direction * SPEED * mass)
			if is_on_floor2():
				apply_central_impulse(Vector3(0, JUMP_WALK_VELOCITY*mass, 0))
			
			
			
		else:
			if is_walking:
				stopped.emit()
				is_walking = false
			#model.animation_player.pause()
	
	while not impulses.is_empty():
		linear_velocity += impulses.pop_back()

	#move_and_slide()

func init_network(player_infos):
	$MultiplayerSynchronizer.add_visibility_filter(
		func(x): 
			return not player_infos.filter(func(y): return y.id == x).is_empty()
	)

const MAX_INCLINATION = PI/4

func is_on_floor() -> bool:
	#print(%FloorArea.get_overlapping_bodies() )
	return not %FloorRays.get_children().filter(func(x): return x.is_colliding() \
			and x.get_collision_normal().dot(Vector3.UP) > cos(MAX_INCLINATION) ).is_empty()
	#return not %FloorArea.get_overlapping_bodies().is_empty()
	
func is_on_floor2() -> bool:
	#print(%FloorArea.get_overlapping_bodies() )
	return not %FloorRays.get_children().filter(func(x): return x.is_colliding() \
			and x.get_collision_normal().dot(Vector3.UP) > cos(MAX_INCLINATION) ).is_empty()
	#return not %FloorArea.get_overlapping_bodies().is_empty()

@rpc("any_peer", "call_local", "unreliable")
func push(impulse: Vector3):
	apply_central_impulse(impulse)

@rpc("any_peer", "call_local", "unreliable")
func stop(duration: float):
	stopped.emit()
	is_stopped = true
	await get_tree().create_timer(duration).timeout
	is_stopped = false
