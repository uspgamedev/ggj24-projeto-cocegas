extends Node
class_name Entity

const ENTITY_GROUP := "ENTITY"

func _notification(what):
	match what:
		NOTIFICATION_READY:
			add_to_group(ENTITY_GROUP)

func get_component(component: Script) -> Node:
	for child in get_children():
		if child.get_script() == component or (child.get_script() and child.get_script().get_base_script() == component):
			return child
	
	return null

func set_node_param(node: String, param_name: String, x):
	if is_instance_valid(get(node)):
		get(node).set(param_name,  x)
	else:
		ready.connect(func(): get(node).set(param_name,  x), CONNECT_ONE_SHOT)

static func get_entity(node: Node) -> Entity:
	var parent = node.get_parent()
	
	while not parent is Entity:
		parent = parent.get_parent()
		
	if parent:
		return parent
	return null
	
static func get_entities() -> Array:
	return Engine.get_main_loop().get_nodes_in_group(ENTITY_GROUP)
	
static func get_singleton(component: Script) -> Node:
	var entities = Engine.get_main_loop().get_nodes_in_group(ENTITY_GROUP) \
			.filter(func(x): return x.get_component(component) != null)
	if entities.size() == 1:
		return entities[0].get_component(component)
	else:
		return null
