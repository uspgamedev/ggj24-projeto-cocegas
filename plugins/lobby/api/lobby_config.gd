extends Resource
class_name LobbyConfig

@export_category("Server Information")

@export var server_address := "127.0.0.1" 
@export var server_port := 8080

@export_category("Security")

@export var use_dtls := true
@export var validate_certificate := true
@export_file var server_private_key_file = "res://plugins/lobby/example/key.key"
@export_file var certificate_file = "res://plugins/lobby/example/cert.crt"
