extends Node

@export_file("*.tscn") var client_test_scene_path := "res://plugins/lobby/test/LobbyClientTest.tscn"

@onready var server: LobbyServer = $LobbyServer

@export var config: LobbyConfig 

func _ready():
	server.init_server()
	await get_tree().create_timer(0.1).timeout

