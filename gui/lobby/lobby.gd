extends Node

var server := false

@export var lobby_config: LobbyConfig = LobbyConfig.new()
@export var debug_lobby_config: LobbyConfig = LobbyConfig.new()

@export var lobby_api_scene: PackedScene
@export var world_scene: PackedScene

var name_list: Array = ["Videoconferencing", "Xulé", "Alcebíades", "Chicharrão", "Chuvisco", "Cuti-Cuti", "Diogo Dias", "Margarida", "Pingüim", "Tradição", "Dingão", "Nem o Vento", "Le Postiche", "Porcelaine", "Natiruts", "Gone Forever", "Fasterpia", "Ask Me How", "Estopim", "Academic", "Pianito's", "Wasted Youth", "Fim De Tarde", "Evolution", "Gentle Voices", "Utopia", "How YOU Doin'", "Stigmatus", "Tchëlis", "Tchënis", "Tchëvis", "Cigane", "Lord Byron", "Chá de Fita", "Olho Seco", "Sente o Drama", "Sente o Tchëlis", "Doces Caseiro", "Ciccarelli", "Allink", "Siscomex", "Halls Preto", "3 por 1 Real", "Jéferson", "Bonito", "Navemãe", "Tchap-tchura", "Ledo Engano", "Zangief"]

@export_file("*.tscn") var server_scene_path := "res://plugins/lobby/example/server.tscn"
#@export_file("*.crt") var certificate_path := "res://plugins/lobby/example/cert.crt"
var debug_certificate_path := "res://plugins/lobby/example/cert.crt"

#@export var server_port := 8080

@onready var join_button: Button = %JoinRoom
@onready var ready_button: Button = %Ready
@onready var room_name_label: Label = %RoomName
@onready var room_name_field: LineEdit = %Room
@onready var entries: RichTextLabel = %Entries
@onready var nickname_field: LineEdit = %Name
@onready var color_field: ColorPickerButton = %Color
@onready var mode_picker: CheckButton = %Mode

enum State {
	TO_JOIN,
	JOINED,
	READY
}

var state = State.TO_JOIN : set = set_state

var lobby_api: LobbyAPI = null

signal update_room(room: LobbyRoom)
signal update_player(info: Dictionary)

func _ready():
	set_state(State.TO_JOIN)
	
	lobby_api = get_node_or_null("/root/LobbyAPI")
	if lobby_api == null:
		lobby_api = lobby_api_scene.instantiate()
		get_tree().root.add_child.bind(lobby_api).call_deferred()
		
	lobby_api.room_updated.connect(update_room_info)
	lobby_api.game_started.connect(start_game)
	join_button.grab_focus()
	
	randomize()
	set_player_info()
	init_joypad()

func set_player_info():
	if PlayerInfo.nickname == "": PlayerInfo.nickname = name_list.pick_random()
	nickname_field.text = PlayerInfo.nickname
	color_field.color = PlayerInfo.color
	update_player_info()

func create_server():
	close_server()
	
	await get_tree().create_timer(0.2).timeout
	
	var server_pid = OS.create_instance([server_scene_path])
	
	var server_pid_file = FileAccess.open("user://server_pid.txt", FileAccess.WRITE)	
	server_pid_file.seek(0)
	server_pid_file.store_64(server_pid)
	server_pid_file.close()

func close_server():
	var server_pid_file = FileAccess.open("user://server_pid.txt", FileAccess.READ)
	if FileAccess.file_exists("user://server_pid.txt") and server_pid_file.get_length() > 4 and not server_pid_file.eof_reached():
		var prev_pid = server_pid_file.get_64()
		OS.kill(prev_pid)

func get_player_info() -> Dictionary:
	return {
		"name": PlayerInfo.nickname,
		"color": PlayerInfo.color.to_html()
	}

func update_room_info(room: LobbyRoom):
	room_name_label.text = room.room_name + " "
	entries.text = ""
	
	for info in room.player_infos.values():
		entries.text += "[color=%s] %s[/color]\n" % [Color.GREEN.to_html() if info.ready else Color.RED.to_html(), info.name]
		if not info.ready and info.id == multiplayer.get_unique_id() and state == State.READY:
			set_state(State.JOINED)
			
	ready_button.disabled = room.player_infos.size() < 2
	update_room.emit(room)
		

func enter_room():
	var config = get_config()
	lobby_api.config = config
	lobby_api.enter_room(room_name_field.text, get_player_info())

func start_game(room: LobbyRoom):
	var world: NetworkedWorld = world_scene.instantiate()
	
	world.room_seed = room.room_seed
	world.local_player_id = lobby_api.multiplayer.get_unique_id()
	world.init_world(room)
	
	add_sibling(world)
	queue_free()
	
func set_state(state_: State):
	match state_:
		State.TO_JOIN:
			color_field.disabled = false
			ready_button.disabled = true
			join_button.disabled = false
			
			nickname_field.editable = true
			room_name_field.editable = true
			
			join_button.text = "Join room"
			ready_button.text = "Ready"
			room_name_label.text = "Room"
			entries.text = ""
			
			
		State.JOINED:
			color_field.disabled = false
			ready_button.disabled = true
			join_button.disabled = false
			
			nickname_field.editable = true
			room_name_field.editable = false
			
			join_button.text = "Leave room"
			ready_button.text = "Ready"
			
		State.READY:
			color_field.disabled = true
			ready_button.disabled = true
			join_button.disabled = false
			
			nickname_field.editable = false
			room_name_field.editable = false
			
			join_button.text = "Leave room"
			ready_button.text = "Unready"
	
	state = state_
		

func _on_join_room_pressed():
	match state:
		State.TO_JOIN:
			enter_room()
			set_state(State.JOINED)
			
		State.JOINED, State.READY:
			lobby_api.leave_room()
			room_name_label.text = "Room"
			set_state(State.TO_JOIN)

func _on_ready_pressed():
	match state:
		State.JOINED:
			lobby_api.set_ready(true)
			set_state(State.READY)
		State.READY:
			lobby_api.set_ready(false)
			set_state(State.JOINED)


func _on_server_pressed():
	create_server()


func get_config() -> LobbyConfig:
	if not mode_picker.button_pressed:
		return debug_lobby_config
	return lobby_config

func get_address() -> String:
	if not mode_picker.button_pressed:
		return "127.0.0.1"
	return "usp.game.dev.br"

func update_player_info():
	PlayerInfo.nickname = nickname_field.text
	PlayerInfo.color = color_field.color
	
	if state != State.READY:
		update_player.emit(get_player_info())
	if state == State.JOINED:
		lobby_api.change_info(get_player_info())

func _update_info(_arg = null):
	update_player_info()

func _input(_event):
	init_joypad()
	
func init_joypad():
	for device in Input.get_connected_joypads():
		var device_info = Input.get_joy_info(device)
		var guid = Input.get_joy_guid(device)
		
		if device_info.get("raw_name", "").to_lower().contains("touchpad"):
			Input.add_joy_mapping(guid + ",Touchpad,platform:Linux", true)

func _exit_tree():
	return
	#get_node("../LobbyAPI").queue_free()
