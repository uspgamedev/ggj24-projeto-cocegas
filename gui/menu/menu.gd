extends Control
class_name Menu

signal paused()
signal unpaused()

@onready var music_bus_id = AudioServer.get_bus_index("Master")
@onready var sfx_bus_id = AudioServer.get_bus_index("SFX")

static func get_menu() -> Menu:
	return Engine.get_main_loop().get_first_node_in_group("MENU")
	
func _notification(what):
	match what:
		NOTIFICATION_WM_WINDOW_FOCUS_IN:
			pause()
			
		NOTIFICATION_WM_WINDOW_FOCUS_OUT:
			pause()

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		#get_viewport().set_input_as_handled()
		if is_paused():
			unpause()
		else:
			pause()


func _on_close_button_pressed():
	unpause()

func pause():
	visible = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func unpause():
	visible = false
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
func is_paused() -> bool:
	return visible
	
func _on_music_slider_value_changed(value):
	AudioServer.set_bus_volume_db(music_bus_id, linear_to_db(value))
	AudioServer.set_bus_mute(music_bus_id, value < 0.05)


func _on_sfx_slider_value_changed(value):
	AudioServer.set_bus_volume_db(sfx_bus_id, linear_to_db(value))
	AudioServer.set_bus_mute(sfx_bus_id, value < 0.05)
