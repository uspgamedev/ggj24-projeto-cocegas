extends Control

var player: Clown

@export var not_clown_icon : Texture2D
@export var clown_icon : Texture2D

func _ready():
	if is_instance_valid(player):
		%Name.text = player.info.name
		self_modulate = player.info.color
	else:
		queue_free()

func _process(_delta):
	if is_instance_valid(player):
		if player.clown:
			%Icon.texture = clown_icon
			%Icon.self_modulate = Color.WHITE
			%Icon2.visible = false
		else:
			%Icon2.visible = true
			%Icon.texture = not_clown_icon
			%Icon.self_modulate = player.info.color
		%Score.text = str(int(round(player.score)))
