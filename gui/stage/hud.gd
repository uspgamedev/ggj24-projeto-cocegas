extends CanvasLayer

@export var rank_scene : PackedScene

var timer: Timer

func add_player(player: Clown):
	var rank = rank_scene.instantiate()
	rank.player = player
	%Ranks.add_child(rank)

func add_timer(timer_: Timer):
	timer = timer_

func show_result(_rank):
	#%Result.text = "Rank " + str(rank)
	#%Result.visible = true
	return

func _process(_delta):
	await get_tree().process_frame
	
	var ranks = %Ranks.get_children()
	ranks.sort_custom(func(x, y): return x.player.score > y.player.score)
	
	while %Ranks.get_child_count() > 0:
		%Ranks.remove_child(%Ranks.get_child(0))
	
	for rank in ranks:
		%Ranks.add_child(rank)
	
	%Timer.text = "%02d:%02d" % [int(timer.time_left/60.), round(int(timer.time_left)%60)]
	
