extends Node3D
class_name SpawnPoints

func get_spawn_position(id: int) -> Vector3:
	if get_children().size() <= id:
		push_error("id > spawn_points")
		return Vector3()
	return get_child(id).position
