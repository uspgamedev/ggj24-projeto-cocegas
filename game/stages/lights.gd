extends Node3D

@export var spot_scene: PackedScene
@export var clown_color := Color.WHITE
@export var luffy_color := Color.WHITE


var spots = []
var players = []

func _on_stage_2_player_added(player):
	var spot = spot_scene.instantiate()
	spots.push_back(spot)
	players.push_back(player)
	add_child(spot)
	look_to_players.call_deferred()
	
func _process(_delta):
	look_to_players()
	
func look_to_players():
	for i in spots.size():
		var spot: Node3D = spots[i]
		var player: Node3D = players[i]
		
		var distance: Vector3 = (player.body.global_position - spot.global_position)
		var up = Vector3.UP
		
		#spot.global_position = player.body.global_position + 25 * distance.normalized() 
		
		if up.cross(player.body.global_position-spot.global_position).length() > 0.01:
			spot.look_at(player.body.global_position, up)
		else:
			spot.rotation = Vector3(-PI/2., -PI/2., PI/2.)
		
			
		if players[i].clown:
			spot.get_child(0).light_color = clown_color
		else:
			spot.get_child(0).light_color = luffy_color
