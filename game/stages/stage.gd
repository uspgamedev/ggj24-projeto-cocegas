extends Node3D
class_name Stage

@export var time := 120.0
@export var END_MATCH_INTERVAL := 5.0

@onready var menu = Menu.get_menu()

var local_player: Clown

signal result(scores: Dictionary)
signal player_added(player: Clown)

func _ready():
	if get_window().has_focus():
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	
	%Timer.start(time)
	$HUD.add_timer(%Timer)

func add_player(player: Node3D, id: int):
	if get_spawn_points().size() <= id:
		push_error("get_spawn_points().size() <= id")
		return
	player.position = get_spawn_points()[id]
	add_child(player)
	%HUD.add_player(player)
	player_added.emit(player)

func get_spawn_points() -> Array:
	for child in get_children():
		if child is SpawnPoints:
			return child.get_children().map(func(x): return x.position)
	
	return []

func _process(_delta):
	check_match_end()

func check_match_end():
	for player in get_tree().get_nodes_in_group("PLAYER"):
		if not player.clown:
			return
	NetworkedMatch.rpc_all(finalize_match)

var finalizing := false

@rpc("any_peer", "call_local", "reliable")
func finalize_match():
	if finalizing:
		return
	finalizing = true
	
	var timer = get_tree().create_timer(END_MATCH_INTERVAL)
	
	var scores = {}
	
	var max_score = 0
	var winners = []
	
	for player in get_tree().get_nodes_in_group("PLAYER"):
		player.process_mode = Node.PROCESS_MODE_DISABLED
		scores[player.info.id] = player.score
		
		if player.score > max_score:
			winners = [player.info.id]
			max_score = player.score
		elif player.score == max_score:
			winners.push_back(player.info.id)
		
	if local_player.has_node("MultiplayerSynchronizer"):
		local_player.get_node("MultiplayerSynchronizer").queue_free()
	
	if winners.find(local_player.info.id) != -1:
		%HUD.show_result(1)
	else:
		%HUD.show_result("∞")
		
	
	timer.timeout.connect(func(): 
		result.emit(scores)
		queue_free()
	)

func _exit_tree():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func _on_timer_timeout():
	finalize_match()

