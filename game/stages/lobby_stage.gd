extends Node3D

@export var model_scene: PackedScene

func _on_lobby_update_room(room: LobbyRoom):
	var idx = 1
	
	$Players.get_children().map(func(x): x.queue_free())
	
	for info in room.player_infos.values():
		if info.id == room.local_id:
			continue
			
		var model: Model = model_scene.instantiate()
		model.position = %SpawnPoints.get_spawn_position(idx)
		idx += 1
		model.rotation.y = 90
		$Players.add_child(model)
		model.set_color(info.color)

func update_player_color(info: Dictionary):
	%Player.set_color(info.color)
