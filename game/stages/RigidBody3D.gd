extends RigidBody3D

func _on_body_entered(body):
	if not body is StaticBody3D:
		be_pushed(body)
	
	if body.is_in_group("pushable_objects"):
		# Call a function to push the rigidbody
		push_rigidbody(body)

func push_rigidbody(rigidbody):
	var push_force = Vector3.ZERO  # Adjust the force as needed
	rigidbody.apply_central_impulse(rigidbody.transform.xform_inv(push_force))

func be_pushed(body):
	apply_central_impulse(body.velocity)
