extends Node3D
class_name World

@export var num_players := 5
@export var stages: Array[PackedScene]
@export var clown_scene: PackedScene

@export_file("*.tscn") var lobby_scene: String

var current_stage := -1
var local_player_id := 0

var game_state := GameState.new()
var room_seed := 19191

func _notification(what):
	if what == NOTIFICATION_READY:
		#add_to_group("WORLD")
		#add_child.call_deferred(GameState.new())
		_next_stage(int(round(room_seed * 93922/112.)) % num_players)
	
func _next_stage(clown_index):
	current_stage += 1

	if current_stage >= stages.size():
		finalize_game()
		return
	
	var stage: Stage = stages[current_stage].instantiate()
	stage.result.connect(finalize_stage)
	add_child.bind(stage).call_deferred()
	
	for i in num_players:
		var player = clown_scene.instantiate()
		if i == clown_index:
			player.clown = true
			
		player.info.name = "Clown " + str(i)
		player.info.id = i
		
		if player.info.id == local_player_id:
			stage.local_player = player
		stage.add_player(player, i)

func finalize_stage(scores):
	game_state.register_stage_result(scores)
	add_sibling(load(lobby_scene).instantiate())
	queue_free()
	#_next_stage(int(round(room_seed * 939212/1232.)) % scores.size())
	#room_seed *= int(321132*room_seed/42933.)
	
func finalize_game():
	pass
