extends World
class_name NetworkedWorld

var room: LobbyRoom

var socket

func init_world(room_: LobbyRoom):
	room = room_
	num_players = room.get_num_players()

func _next_stage(clown_index):
	var stage: Stage = stages[0].instantiate()
	stage.result.connect(finalize_stage)
	stage.name = "Stage"
	
	for i in room.player_infos.size():
		var player = clown_scene.instantiate()
		if i == clown_index:
			player.clown = true
			
		player.name = str(room.player_infos.values()[i].id)
		player.info = room.player_infos.values()[i]
		
		if player.info.id == local_player_id:
			stage.local_player = player
		
		player.set_multiplayer_authority(room.player_infos.values()[i].id)
		
		if i == clown_index:
			stage.add_player(player, 0)
		elif i == 0 and i != clown_index:
			stage.add_player(player, clown_index)
		else:
			stage.add_player(player, i)
			

	add_child(stage)
