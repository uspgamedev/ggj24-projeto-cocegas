extends Node
class_name NetworkedMatch

@export var world: NetworkedWorld

var room: LobbyRoom

func _ready():
	multiplayer.peer_disconnected.connect(remove_peer)
	room = world.room

func remove_peer(peer_id):
	room.remove_player_info(peer_id)

static func rpc_all(callable: Callable):
	var this: NetworkedMatch = Entity.get_singleton(NetworkedMatch)
	for id in Entity.get_singleton(NetworkedMatch).room.player_infos.keys():
		if this.multiplayer.get_peers().has(id): 
			callable.rpc_id(id)
	callable.call()
		
static func rpc_others(callable: Callable):
	var this: NetworkedMatch = Entity.get_singleton(NetworkedMatch)
	for id in this.room.player_infos.keys():
		if id != this.multiplayer.get_unique_id() and this.multiplayer.get_peers().has(id): 
			callable.rpc_id(id)
