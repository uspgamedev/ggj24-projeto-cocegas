extends Node
class_name SyncTest

enum Mode {
	PLAYER,
	OBJECT
}

@export var mode :=  Mode.OBJECT 
@export var nodes_to_sync: Array[Node] = []
@export var properties_to_sync: Array[String] = []

var player_infos: Array = []

@export var replication_interval := 0.04

@onready var entity := Entity.get_entity(self)

func _ready():
	if multiplayer.multiplayer_peer == null:
		queue_free()
	
	get_tree().create_timer(replication_interval).timeout.connect(func(): self.sync.call_deferred())
	
	if Entity.get_singleton(NetworkedMatch):
		player_infos = Entity.get_singleton(NetworkedMatch).room.get_infos_ordered()
	
	match mode:
		Mode.PLAYER:
			pass
		Mode.OBJECT:
			# TODO: consensus?
			update_authority()
	
func update_authority():
	if mode == 2:
		var pos = entity.get_component(Body).global_position
		var distance = -1 
		var nearest
		
		for player in get_tree().get_nodes_in_group("PLAYER"):
			var dist = (player.get_component(Body).global_position - pos).length()
			if distance == -1 or dist < distance:
					nearest = player.get_multiplayer_authority()
					distance = dist 
		
		entity.set_multiplayer_authority(nearest)

func sync():
	update_authority()
	get_tree().create_timer(replication_interval).timeout.connect(func(): self.sync.call_deferred())	
	
	if not is_multiplayer_authority():
		return
	
	var properties := properties_to_sync.duplicate()
	var property = properties.pop_front()

	for node_idx in nodes_to_sync.size():
		var node = nodes_to_sync[node_idx]
		if property == null:
			break
			
		while property != null and not property.is_empty():
			NetworkedMatch.rpc_others(receive_sync.bind(node_idx, property, node.get(property)))
			property = properties.pop_front()
	
		if property != null and property.is_empty():
			property = properties.pop_front()


# TODO: remove authority? and manually check the authority
@rpc("authority", "call_local", "unreliable")
func receive_sync(node_idx: int, property: String, value):
	nodes_to_sync[node_idx].set(property, value)

func filter(id) -> bool:
	return not player_infos.filter(func(y): return y.id == id and y.id != multiplayer.get_unique_id()).is_empty()

#@rpc("authority", "call_local", "reliable")
#func set_auth(node_idx: int, property: String, value):
	#nodes_to_sync[node_idx].set(property, value)

#var replication_config := SceneReplicationConfig.new()
#func config():
	#var properties := properties_to_sync.duplicate()
	#var property = properties.pop_front()
#
	#for node in nodes_to_sync:
		#if property == null:
			#break
			#
		#while property != null and not property.is_empty():
			#var node_path = str(self.get_path_to(node)).right(-3)
			#if node_path.is_empty():
				#node_path = "."
				#
			#var property_path = node_path + ":" + property
			#
			#if replication_config.get_properties().map(func(x): return str(x)).find(property_path) == -1:
				#replication_config.add_property(property_path)
			#property = properties.pop_front()
	#
		#if property != null and property.is_empty():
			#property = properties.pop_front()
	
