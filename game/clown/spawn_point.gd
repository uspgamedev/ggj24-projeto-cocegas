@tool
extends MeshInstance3D

func _ready():
	if not Engine.is_editor_hint():
		visible = false
	else:
		set_color.call_deferred()

func set_color():
		var material = StandardMaterial3D.new()
		
		if get_parent().get_children().find(self) == 0:
			material.albedo_color = Color.RED
		else:
			material.albedo_color = Color.BLUE
			
		material_override =  material

