extends Entity
class_name Clown

@export var SCORE_RATE := 2.0
@export var TICKLE_SCORE := 50.0

@export var clown := false : set = set_clown

@export var cocegas_scene : PackedScene
@export var clown_scene : PackedScene

var info: Dictionary

var stop = false
var score := 0.0

@onready var clown_model := %Clown
@onready var normal_model := %Model

@onready var models = [clown_model, normal_model]

@onready var model: Model

@onready var body: TiltingDoll = %Body


func _ready():
	update_model()
	init_body()


var prev_clown = false

func _physics_process(delta):
	if not clown: update_player_score(delta)
	update_skeleton()


func update_player_score(delta):
	var rate = SCORE_RATE * len(get_tree().get_nodes_in_group("PLAYER").filter(func(player): return player.clown))
	score += rate * delta


var head_rotation = 0.

func update_skeleton():
	if is_multiplayer_authority():
		head_rotation = body.camera_pivot.rotation.x
	model.set_head_rotation(head_rotation)	
	
	
@rpc("any_peer", "call_local", "reliable")
func tickle():
	stop = true
	%TickleAudio.play()
	# await animation.play()
	await create_tween().tween_interval(1.0).finished
	stop = false
	if is_multiplayer_authority():
		score += TICKLE_SCORE


@rpc("any_peer", "call_local", "reliable")
func be_tickled():
	body.stop(1.0)
	# await animation.play()
	await create_tween().tween_interval(1.0).finished
	clown = true


func set_clown(b: bool):
	clown = b
	
	if is_inside_tree():
		update_model()
	
	
func update_model():
	clown_model.visible = false
	normal_model.visible = false
	model = clown_model if clown else normal_model
	model.visible = true
	
	%TickleArea.monitoring = clown
	
	
func init_body():
	normal_model.set_color(info.color)
	for mod in models:
		$Body/PushArea.start_pushing.connect(func(): NetworkedMatch.rpc_all(mod.play.bind(Model.Anim.Push)))
		body.start_walking.connect(func(): NetworkedMatch.rpc_all(mod.play.bind(Model.Anim.Walk)))
		body.stopped.connect(func(): NetworkedMatch.rpc_all(mod.pause))
	
	
	
	
