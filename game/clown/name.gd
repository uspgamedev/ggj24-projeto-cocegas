extends Label3D

func _ready():
	if is_multiplayer_authority():
		queue_free()
	else:
		text = get_parent().get_parent().info.name

