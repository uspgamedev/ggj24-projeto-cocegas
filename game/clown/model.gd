extends Node3D
class_name Model

@export var skin_material: ShaderMaterial
@export var skeleton: Skeleton3D

@export var has_ears := false

enum Anim {
	Walk,
	Idle,
	Push
}

@export var animation_player: AnimationPlayer

@rpc("any_peer", "call_local", "unreliable")
func play(anim: Anim):
	var anim_name
	
	match anim:
		Anim.Walk:
			anim_name = "player/walk"
		Anim.Idle:
			anim_name = "player/idle"
		Anim.Push:
			anim_name = "player/push"
	
	if animation_player.has_animation(anim_name) and (animation_player.current_animation != anim_name or not animation_player.is_playing()):
		animation_player.play(anim_name)

@rpc("any_peer", "call_local", "reliable")
func pause():
	animation_player.pause()

@rpc("any_peer", "call_local", "reliable")
func stop():
	animation_player.stop()

func set_color(color: Color):
	var material = skin_material.duplicate()
	material.set_shader_parameter("color_1", color)
	get_tree().get_nodes_in_group("BODY")\
		.filter(func(x): return is_ancestor_of(x))\
		.map(func(x: MeshInstance3D): x.set_surface_override_material(0, material))
		
@onready var initial_rotation = skeleton.get_bone_pose_rotation(20)
@onready var initial_rotation2 = null if skeleton.get_node_or_null("HeadBone") == null else skeleton.get_node("HeadBone").quaternion

func set_head_rotation(angle):
	var current_rotation = initial_rotation*Quaternion(Vector3.FORWARD, -angle)
	skeleton.set_bone_pose_rotation(20, current_rotation)
	if skeleton.get_node_or_null("HeadBone") != null:
		skeleton.get_node("HeadBone").quaternion = initial_rotation2 * \
				#Quaternion(Vector3.UP, self.global_rotation.y)*
				Quaternion(Vector3.FORWARD, -angle)
		
