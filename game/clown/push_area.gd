extends Area3D

@export var PUSH_IMPULSE := 200.

@onready var body := %Body

signal start_pushing()

@export var model: Node

var pushing = false

func _unhandled_input(event):
	if not is_multiplayer_authority() or Menu.get_menu().is_paused():
		return
			
	if event.is_action_pressed("push") and pushing == false:
		pushing = true
		start_pushing.emit()
		get_tree().create_timer(0.4).timeout.connect(NetworkedMatch.rpc_all.bind(push))
		get_tree().create_timer(0.7).timeout.connect(func(): self.pushing = false)

@rpc("any_peer", "call_local", "unreliable")
func push():
	
	get_node("PushAudio"+str(randi()%3+1)).play()
	if not is_multiplayer_authority():
		return
	
	for b in get_overlapping_bodies():
		#if b is Body and b != body:
			#NetworkedMatch.rpc_all(b.push.bind((b.global_position - body.global_position).normalized() * PUSH_IMPULSE))
		if b is Body and b != body:
			NetworkedMatch.rpc_all(b.push.bind((b.global_position - body.global_position).normalized() * PUSH_IMPULSE))









