extends Node

enum Mode {
	MOUSE_KEYBOARD,
	JOYSTICK
}

@export var mode := Mode.MOUSE_KEYBOARD
@export var actions : Array[String] = [""]

# -------------------------- API --------------------------

func is_paused() -> bool:
	return false

func pop_mouse_motion():
	return Vector2()
	
func just_pressed(_action: String) -> bool:
	return false

func get_vector() -> Vector2:
	return Vector2()


