extends Area3D

@onready var entity := Entity.get_entity(self)

func _on_body_entered(body):
	var body_entity = Entity.get_entity(body)
	if body_entity is Clown and body_entity != entity and entity.clown and \
		not body_entity.clown and not body_entity.stop:
		NetworkedMatch.rpc_all(entity.tickle)
		if body is Body and body.get_parent() is Clown:
			NetworkedMatch.rpc_all(Entity.get_entity(body).be_tickled)
