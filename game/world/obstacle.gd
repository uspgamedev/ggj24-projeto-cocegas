@tool
extends Entity

@onready var types := %Types
@export var type := 0 : set = set_type
func set_type(x):
	type = x
	set_node_param("types", "type", x)

