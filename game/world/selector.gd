@tool
extends Node3D

@export var type := 0 : set = set_type

func _ready():
	set_type(type)

func set_type(i):
	for variant in get_children():
		variant.visible = false
	if i >= 0 and i < get_children().size():
		get_child(i).visible = true
	type = i
