extends Node3D

@export var body: Node3D
@export var MAX_VERTICAL_ANGLE := 30 
@export var VERTICAL_SENSITIVITY := 0.1

@onready var camera = %Camera3D

@onready var ray: RayCast3D = %RayCast3D

var vrot = 0.

func _ready():
	if not is_multiplayer_authority():
		self.queue_free()
		
func _unhandled_input(event):
	if event is InputEventMouseMotion:
		vrot += deg_to_rad(-event.relative.y * VERTICAL_SENSITIVITY)
		vrot = clampf(vrot, -deg_to_rad(MAX_VERTICAL_ANGLE), deg_to_rad(MAX_VERTICAL_ANGLE))

var wall = null

func _process(delta):
	var movement = Input.get_axis("camera_down", "camera_up")
	
	vrot += deg_to_rad(-movement * 60. * delta)
	vrot = clampf(vrot, -deg_to_rad(MAX_VERTICAL_ANGLE), deg_to_rad(MAX_VERTICAL_ANGLE))
	
func _physics_process(_delta):
	global_rotation.x = vrot
	global_rotation.z = 0
	global_rotation.y = body.global_rotation.y

	if wall != null:
		wall.visible = true

	if ray.is_colliding():
		wall = ray.get_collider()
		wall.visible = false
		
	
	
	
	
	
