extends Node3D

@export var subdivisions := 1

func _ready():
	divide.call_deferred()

func divide():
	var mesh: MeshInstance3D = get_child(0)

	var old_width = mesh.mesh.size.x
	var new_width = old_width/subdivisions
	
	for i in subdivisions:
		var new_mesh = mesh.duplicate()
		new_mesh.basis = mesh.basis
		new_mesh.scale.x /= subdivisions
		new_mesh.position.x += (i+0.5)*new_width - old_width/2
		
		add_child(new_mesh)
	
	mesh.queue_free()
	
